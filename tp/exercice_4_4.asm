#Déclaration des données
	.data
chaine:	.asciiz	"eve e"
str:	.asciiz	"e"

	.text
	.globl __start
	
#programme principal
__start:
	jal main				# saut dans le main
	#exit
	li $v0, 10				# charge valeur 10 pour syscall
	syscall					# exit

main:
	la $t5, chaine				#on charge l'adresse du première élément de chaine dans $t5
	la $t6, str				#on charge l'adresse du première élément de str dans $t6
	la $t7, str				#charge l'adresse du première élément de str dans $t7
	li $t0, 0				#charge la valeur 0 dans $t0
	sub $t6,$t6,1				#soustraction de 1 de $t6 pour arriver à la dernière valeur mémoire de chaine
while2: 	beq $t5, $t6, endwhile2		#si $t5 = $t6 on saute à endwhile2
	lb $t1, ($t5)				#on charge dans $t1 la valeur à l'adresse mémoire $t5
	lb $t2, ($t7)				#on charge dans $t2 la valeur à l'adresse mémoire $t7
if: 	bne $t1, $t2, endif			#si $t1 != $t2 on saute à endif sinon on continu
then:	add $t0, $t0, 1				#on incrémente $t0 de 1
endif:
	add $t5, $t5, 1				#on incrémente $t5 de 1
	b while2				#on saute à while2
endwhile2:
	move $a0, $t0				#on affecte à $a0 la valeur dans $t0
	li $v0, 1				#on charge la valeur 1 pour syscall
	syscall					#on affiche valeur dans $a0
	li $v0, 0				#on charge la valeur 0 dans dans $v0
	jr $ra					# fin du main